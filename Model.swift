//
//  Model.swift
//  Tarot
//
//  Created by Rodney Cocker on 29/02/2016.
//  Copyright © 2016 RMIT. All rights reserved.
//

import Foundation

class Model
{
  let majorArcanaCards = ["Fool" : "Go forth with faith.",
    "Magician" : "Magic is on your side.",
    "HighPriestess" : "A powerful woman will be part of the solution.",
    "Empress" : "Plant the seed and it will grow.",
    "Emperor" : "You will need to be well organised.",
    "Heirophant" : "A powerful man will be part of the solution.",
    "Lovers" : "You cannot do this alone.",
    "Chariot" : "You need to find the right vehicle to achieve the outcome.",
    "Justice" : "If you have been fair in your dealings with others, you have nothing to be concerned about.",
    "Hermit" : "This is not the time to take action, but more study and reflection is required.",
    "WheelOfFortune" : "The outcome has already been decided.",
    "Strength" : "You have the skills and abilities required to pull this off.",
    "HangedMan" : "Try looking at the situation from another perspective.",
    "Death" : "You must finish something first, before you can move forward.",
    "Temperance" : "A balanced approach is need for a successful outcome.",
    "Devil" : "The devil is in the details, make sure you have made every effort for success.",
    "Tower" : "Something unexpected will happen that will resolve the situation.",
    "Star" : "Look to the stars, you will find your answer.",
    "Moon" : "The answer is not known at this time, please ask again later.",
    "Sun" : "Relax, it is done.",
    "Judgement" : "Be discerning and consult others before making a decision.",
    "World" : "The result you are looking for has already happened." ]
  
  var currentCardImageName = "";
  
  // Return the oracles response to the question posed by the user
  func respond()->String
  {
    // Returns a random integer within the range of indexes for the answers array
    let response: Int = Int(arc4random_uniform(UInt32(majorArcanaCards.count)))
    
    // Convert the values in the dictionary to an Array and retrieve the value at the specified index
    let message = Array(majorArcanaCards.values)[response]
    
    // Set the current card name based on the specified index
    currentCardImageName = Array(majorArcanaCards.keys)[response]
    
    return message
  }
}

