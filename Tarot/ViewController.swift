//
//  ViewController.swift
//  Tarot
//
//  Created by Rodney Cocker on 29/02/2016.
//  Copyright © 2016 RMIT. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
  // Property referencing the label in the view
  @IBOutlet weak var lblAnswers: UILabel?
  
  // Property referencing the model for managing data and business logic
  var model = Model()
  
  // Return the Oracle's response to the question posed by the user
  func respond(inout cardImageName:String, inout message:String)
  {
    // Returns a random integer within the range of indexes for the answers array
    let response: Int = Int(arc4random_uniform(UInt32(majorArcanaCards.count)))
    
    // Convert the values in the dictionary to an Array and retrieve the value
    // at the specified index
    message = Array(majorArcanaCards.values)[response]
    
    // Set the current card name based on the specified index
    cardImageName = Array(majorArcanaCards.keys)[response]
  }
  
  // Lifecycle method for performing tasks after the view has loaded
  override func viewDidLoad()
  {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  // Lifecycle method for clearing up memory resources
  override func didReceiveMemoryWarning()
  {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}